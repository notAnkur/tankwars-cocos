// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    public speed: number = 2;
    @property
    public extremes: number = 360;
    @property
    public verticalDist: number = 20;
    @property
    public enemyPoints: cc.Node[] = [];

    private lastShootTime: number = 0;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.enemyPoints = this.node.children;
    }

    update (deltaTime: number) {
        let newpos = cc.v2(this.node.position.x + this.speed, this.node.position.y);
        const timeNow = Date.now();

        if(this.node.position.x <= -this.extremes) {
            this.speed  = Math.abs(this.speed);
            newpos.y -= this.verticalDist;
        }
        if(this.node.position.x >= this.extremes) {
            this.speed  = Math.abs(this.speed) * -1;
            newpos.y -= this.verticalDist;
        }

        this.node.position = cc.v3(newpos.x, newpos.y, this.node.position.z);
        
        if(this.lastShootTime==0 || timeNow-this.lastShootTime > 5000) {
            let rand = Math.floor(Math.random() * (this.node.childrenCount));
            this.node.children[rand].getComponent('Enemy').shoot();
            this.lastShootTime = Date.now();
        }
    }
}
