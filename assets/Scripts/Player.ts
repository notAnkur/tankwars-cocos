// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    healthLabel: cc.Label = null;
    @property(cc.Prefab)
    bullet: cc.Prefab = null;
    @property
    health: number = 3;
    @property
    timeBetweenShots: number = 2000;
    @property(cc.Node)
    gm: cc.Node = null;
    @property(cc.ProgressBar)
    reloadIndicator: cc.ProgressBar = null;

    private lastShootTime: number = 0;


    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    }

    start () {
        
    }

    setInput () {

    }

    onKeyDown (event) {
        let direction;
        switch(event.keyCode) {
            case cc.macro.KEY.a:
                direction = cc.v2(this.node.position.x - 30, this.node.position.y);
                this.node.position = cc.v3(direction.x, direction.y, this.node.position.z);
                break;
            case cc.macro.KEY.d:
                direction = cc.v2(this.node.position.x + 30, this.node.position.y);
                this.node.position = cc.v3(direction.x, direction.y, this.node.position.z);
                break;
            case cc.macro.KEY.space:
                this.shoot();
                break;
        }
    }

    shoot () {
        const timeNow = Date.now();
        if(this.lastShootTime==0 || timeNow-this.lastShootTime > this.timeBetweenShots) {
            let newBullet = cc.instantiate(this.bullet);
            let playerPos = cc.v2(this.node.position.x, this.node.position.y);
            newBullet.setPosition(playerPos.x, playerPos.y);
            console.log(playerPos.x, "----", newBullet.position.x);
            console.log(this.node.position);
            console.log(newBullet.position);
            this.node.parent?.getChildByName("BarrelPosition")?.addChild(newBullet);
            this.lastShootTime = Date.now();
        }
    }

    update (dt) {
        const timeDiff: number = Date.now() - this.lastShootTime;
        if(timeDiff < this.timeBetweenShots) {
            this.reloadIndicator.progress = timeDiff / this.timeBetweenShots;
        }
    }

    onCollisionEnter (otherCollider, selfCollider) {
        otherCollider.node.destroy();
        this.damage();
    }

    damage () {

        this.health --;

        this.healthLabel.string = `X ${this.health}`;
        if(this.health < 1) {
            this.gm.getComponent("GameManager").GameOver(false);
            this.node.destroy();
        }
    }
}
