// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    scoreLabel: cc.Label = null;
    @property(cc.Label)
    gameOverLabel: cc.Label = null;
    @property(cc.Node)
    gameOverScreen: cc.Node = null;
    @property(cc.Label)
    gameOverScoreLabel: cc.Label = null;

    private score: number = 0;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.updateScore(0);
    }

    // update (dt) {}

    updateScore (sc: number) {
        this.score += sc;
        this.scoreLabel.string = this.score.toString();
    }

    // true -> player destroyed all the enemies
    // false -> player lost
    GameOver (status: boolean) {
        this.gameOverScreen.active = true;
        cc.game.pause();
        if(status) {
            this.gameOverLabel.string = "You Won!!";
        } else {
            this.gameOverLabel.string = "You Lost :(";
        }
        this.gameOverScoreLabel.string = `Your score: ${this.score}`;
    }
}
