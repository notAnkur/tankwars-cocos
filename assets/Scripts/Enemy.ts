// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    // @property(cc.Label)
    // label: cc.Label = null;

    @property
    health: number = 1;
    @property
    scoreValue: number = 1;
    @property(cc.Prefab)
    bullet:cc.Prefab = null;
    @property(cc.Node)
    gm: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        // manager.enabledDebugDraw = true;
        // manager.enabledDrawBoundingBox = true;
    }

    start () {
        
    }

    // update (dt) {}

    onCollisionEnter (otherCollider, selfCollider) {
        console.log(otherCollider.name)
        if(otherCollider.name === "GameOverTrigger<BoxCollider>") {
            this.gm.getComponent("GameManager").GameOver(false);
            return;
        }
        otherCollider.node.destroy();
        this.damage();
    }

    updateScore () {
        this.gm.getComponent("GameManager").updateScore(this.scoreValue);
    }

    damage () {
        this.health --;
        if(this.health <= 0) {
            this.updateScore();
            if(this.node.parent.childrenCount <= 1) {
                this.gm.getComponent("GameManager").GameOver(true);
            }
            this.node.destroy();
        }
    }

    shoot() {
        const parentPos = this.node.parent.position;
        const enemyPos = this.node.position;
        let shootPos = cc.v2(enemyPos.x + parentPos.x, enemyPos.y + parentPos.y);

        let newBullet = cc.instantiate(this.bullet);
        newBullet.setPosition(shootPos.x, shootPos.y);
        console.log(newBullet.position)
        this.node.parent?.parent?.getChildByName("EnemyBulletHolder")?.addChild(newBullet);
    }
}
