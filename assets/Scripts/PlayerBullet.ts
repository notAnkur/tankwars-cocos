// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property
    speed: number = 10;

    public startTime: number = Date.now();

    start () {
        
        this.startTime = Date.now();
    }

    onCollisionEnter (otherCollider, selfCollider) {
        if(otherCollider.node.group.toLowerCase() === "environment") {
            otherCollider.node.destroy();
            this.node.destroy();
        }
    }

    update (deltaTime: number) {
        let newpos = cc.v3(this.node.position.x, this.node.position.y);
        let currTime = Date.now();
        newpos.y += this.speed;
        this.node.position = newpos;

        if(currTime - this.startTime >= 10000) {
            this.node.destroy();
        }
    }
}
